package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import empresaTransporte.Destino;
import empresaTransporte.Flete;
import empresaTransporte.MegaTrailer;
import empresaTransporte.Paquete;
import empresaTransporte.TrailerComun;
import empresaTransporte.Transporte;

public class TestEquals {
	Destino dest;
	Destino destDePrueba;
	Paquete paquete;
	Paquete paqueteDePrueba;
	Transporte transporte;
	Transporte transporteDePrueba;
	
	@Before
	public void setUp() throws Exception {
		dest=new Destino("Buenos Aires",100);
		paquete=new Paquete("Buenos Aires",100,3,true);
		transporte=new TrailerComun("AA333XQ", 10000, 60, true, 2, 100);
	}

	//Equals de Destinos
	
	@Test
	public void equalsMismoDestinoMismoKmTest() {
		destDePrueba=new Destino("Buenos Aires",100);
		assertEquals(dest,destDePrueba);
	}
	@Test
	public void equalsMismoDestinoDistintoKmTest() {
		destDePrueba=new Destino("Buenos Aires",200);
		assertNotEquals(dest,destDePrueba);
	}
	@Test 
	public void equalsDistintoDestinoMismoKmTest() {
		destDePrueba=new Destino("Rosario", 100);
		assertNotEquals(dest, destDePrueba);
	}
	@Test 
	public void equalsDistintoDestinoDistintoKmTest() {
		destDePrueba=new Destino("Rosario", 200);
		assertNotEquals(dest, destDePrueba);
	}
	@Test
	public void equalsDestinoNullTest() {
		destDePrueba=null;
		assertNotEquals(dest, destDePrueba);
	}
	@Test(expected = RuntimeException.class)
	public void equalsMismoDestinoKmNegativoTest() {
		destDePrueba=new Destino("Buenos Aires",-100);
	}
	
	//Equals de Paquetes
	
	@Test 
	public void equalsMismoPaqueteTest() {
		paqueteDePrueba=new Paquete("Buenos Aires",100,3,true);
		assertEquals(paquete, paqueteDePrueba);
	}
	@Test 
	public void equalsPaqueteDistintoTest() {
		paqueteDePrueba=new Paquete("Rosario",200,5,false);
		assertNotEquals(paquete, paqueteDePrueba);
	}
	@Test 
	public void equalsPaqueteNullTest() {
		paqueteDePrueba=null;
		assertNotEquals(paquete, paqueteDePrueba);
	}
	@Test(expected = RuntimeException.class)
	public void equalsMPaqueteValoresNegativosTest() {
		paqueteDePrueba=new Paquete("Rosario",-200,-5,false);
	}
	
	//Equals de Transporte
	
	@Test 
	public void equalsMismoTransporteTest() {
		transporteDePrueba=new TrailerComun("AA333XQ", 10000, 60, true, 2, 100);
		transporte.asignarUnDestino(dest);
		transporteDePrueba.asignarUnDestino(dest);
		transporte.cargarPaquete(paquete);
		transporteDePrueba.cargarPaquete(paquete);
		assertEquals(transporte, transporteDePrueba);
	}
	@Test 
	public void equalsTransportesIgualesTest() {
		transporteDePrueba=new TrailerComun("QW516MO", 23000, 90, false, 4, 150);
		transporte.asignarUnDestino(dest);
		transporteDePrueba.asignarUnDestino(dest);
		transporte.cargarPaquete(paquete);
		transporteDePrueba.cargarPaquete(paquete);
		assertEquals(transporte, transporteDePrueba);
	}
	@Test 
	public void equalsDistintoTipoDeTransporteTest() {
		transporteDePrueba=new Flete("AB555MN", 5000, 20, 4, 2, 300);
		assertNotEquals(transporte, transporteDePrueba);
	}
	@Test 
	public void equalsMismoTipoDeTransporteDistintoDestTest() {
		transporteDePrueba=new TrailerComun("QW516MO", 23000, 90, false, 4, 150);
		destDePrueba=new Destino("Rosario", 100);
		transporte.asignarUnDestino(dest);
		transporteDePrueba.asignarUnDestino(destDePrueba);
		assertNotEquals(transporte, transporteDePrueba);
	}
	@Test 
	public void equalsMismoTipoDeTransporteDistintaCargaTest() {
		transporteDePrueba=new TrailerComun("QW516MO", 23000, 90, false, 4, 150);
		paqueteDePrueba=new Paquete("Buenos Aires",300,2,false);
		transporte.asignarUnDestino(dest);
		transporteDePrueba.asignarUnDestino(dest);
		transporte.cargarPaquete(paquete);
		transporteDePrueba.cargarPaquete(paqueteDePrueba);
		assertNotEquals(transporte, transporteDePrueba);
	}
	@Test 
	public void equalsTransporteNullTest() {
		transporteDePrueba=null;
		assertNotEquals(transporte, transporteDePrueba);
	}
	@Test (expected = RuntimeException.class)
	public void equalsTransporteValoresNegativosFleteTest() {
		transporteDePrueba=new Flete("AB555MN", -5000, -20, -4, -2, -300);
		assertNotEquals(transporte, transporteDePrueba);
	}
	@Test (expected = RuntimeException.class)
	public void equalsTransporteValoresNegativosTrailerTest() {
		transporteDePrueba=new TrailerComun("AB555MN", -5000, -20, true, -300,-200);
		assertNotEquals(transporte, transporteDePrueba);
	}
	@Test (expected = RuntimeException.class)
	public void equalsTransporteValoresNegativosMegaTrailerTest() {
		transporteDePrueba=new MegaTrailer("AB555MN", -5000, -20, true, -300,-200,-90,-4);
		assertNotEquals(transporte, transporteDePrueba);
	}
}
