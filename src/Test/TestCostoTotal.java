package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import empresaTransporte.Empresa;

public class TestCostoTotal {
	Empresa emp;
	double costoVieje;

	
	@Before
	public void setUp() throws Exception {
		emp = new Empresa("30112223334", "Expreso Libre", 35000);
		
	}
	//Costo de Mega Trailer
	
	@Test
	public void costoKmMegaTrailer() {
		emp.agregarDestino("San Luis", 1100);
		emp.agregarMegaTrailer("NK884QE", 15500, 300, false, 7, 200, 625, 70);
		emp.asignarDestino("NK884QE", "San Luis");
		emp.incorporarPaquete("San Luis", 80, 2.0, false);
		emp.incorporarPaquete("San Luis", 265, 7, false);
		emp.incorporarPaquete("San Luis", 39, 4, false);
		emp.cargarTransporte("NK884QE");
		emp.iniciarViaje("NK884QE");
		costoVieje=emp.obtenerCostoViaje("NK884QE");
		assertEquals(8595.0,costoVieje,0.5);	
	}
	@Test
	public void test() {
		emp.agregarDestino("Rosario", 500);
		emp.agregarMegaTrailer("GH854EU", 15500, 10, false, 0, 0, 0, 0);
		emp.asignarDestino("GH854EU", "Rosario");
		emp.incorporarPaquete("Rosario", 80, 2.0, false);
		emp.incorporarPaquete("Rosario", 265, 7, false);
		emp.incorporarPaquete("Rosario", 39, 4, false);
		emp.cargarTransporte("GH854EU");
		emp.iniciarViaje("GH854EU");
		costoVieje=emp.obtenerCostoViaje("GH854EU");
		assertEquals(0.0,costoVieje,0.0);	
	}
	@Test(expected = RuntimeException.class)
	public void costoKmMegaTrailerNegativo() {
		emp.agregarDestino("Rosario", 500);
		emp.agregarMegaTrailer("GH854EU", 15500, 10, false,-10, -9, -500, -70);
	}
}

