package empresaTransporte;


public class Flete extends Transporte {
	
	private double costoFijoAcomp;
    private int acompaniantes;


    public Flete() {
    }
    public Flete(String matricula, double cargaMax, double capacidad, double costoKm, int cantAcompaniantes, double costoPorAcompaniante) {
    	super(matricula,cargaMax,capacidad,false,costoKm);
    	acompaniantes=cantAcompaniantes;
    	costoFijoAcomp=costoPorAcompaniante;
    	invarianteRep();
    }
    private void invarianteRep() {
    	if(!(0<costoFijoAcomp))
    		throw new RuntimeException("Se ha ingresado un costo fijo de acompa�ante invalido");
    	if(!(0<=acompaniantes))
    		throw new RuntimeException("Se ha ingresado una cantidad de acompa�ante invalido");
    	}
    @Override
    public double costoTotalViaje() {
    	if(enViaje) {
    		return super.costoTotalViaje()+(costoFijoAcomp*acompaniantes);
    	}
    	else {
    		throw new RuntimeException("El trasnporte no est� en viaje");
    	}
    }
}