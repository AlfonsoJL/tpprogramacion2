package empresaTransporte;

public class DepositoSinRefrigeracion extends Deposito {

	public DepositoSinRefrigeracion(double capacidadMaxDep, boolean refrigeracion) {
		super(capacidadMaxDep, refrigeracion);
	}
	@Override
	public String toString() {
		StringBuilder st=new StringBuilder();
		st.append(", ");
		st.append(this.getClass().getSimpleName());
		st.append(", tiene como paquetes: ");
		st.append(paquetes);
		return st.toString();
	}
}
