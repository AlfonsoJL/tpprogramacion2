package empresaTransporte;


public class Destino {
	private String destino;
    private double distanciaKm;

    public Destino() {
    }
    public Destino(String destino, int distancia) {
    	this.destino=destino;
    	distanciaKm=distancia;
    	invarianteRep();
    }
    private void invarianteRep() {
    	char caracter;
		for(int i=0; i< destino.length();i++) {
    		caracter = destino.charAt(i);
    		if(!((caracter>='A'&& caracter<='Z')||(caracter>='a'&& caracter<='z')||caracter==' '))
    			throw new RuntimeException("Se ha ingresado un nombre destino invalido");
		}
	  		
    	if(!(0<distanciaKm))
    		throw new RuntimeException("Se ha ingresado una distancia de destino invalido");
    }    
    
    public double darDistanciaKm() {
        return  distanciaKm;
    }
    public String darDestino() {
        return destino;
    }
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		long temp;
		temp = Double.doubleToLongBits(distanciaKm);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Destino other = (Destino) obj;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (Double.doubleToLongBits(distanciaKm) != Double.doubleToLongBits(other.distanciaKm))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder st=new StringBuilder();
		st.append(", Destino: ");
		st.append(destino);
		st.append(", Km: ");
		st.append(distanciaKm);
		return st.toString();
	}
    
}