package empresaTransporte;
import java.util.regex.*;
import java.util.HashSet;

public abstract class Transporte {
	private String matricula;
    private double cargaMaxPeso;
    private double cargaMaxVol;
    private boolean refrigeracion;
    private double costoKmViaje;
    private boolean conCarga;
    protected Destino destinoDelTransporte;
    protected boolean enViaje;
    private double cargaMaxPesoActual;
    private double cargaMaxVolActual;
    private HashSet <Paquete> paquetes;

    public Transporte() {
    }
    public Transporte(String matricula, double cargaMaxPeso, double cargaMaxVol, boolean refrigeracion, double costoKmViaje) {
    	this.matricula=matricula;
    	this.cargaMaxPeso=cargaMaxPeso;
    	this.cargaMaxVol=cargaMaxVol;
    	this.refrigeracion=refrigeracion;
    	this.costoKmViaje=costoKmViaje;
    	conCarga=false;
    	destinoDelTransporte=null;
    	enViaje=false;
    	cargaMaxPesoActual=0.0;
    	cargaMaxVolActual=0.0;
    	paquetes=new HashSet <Paquete>();
    	invarianteRep();
    }
    private void invarianteRep() {//AA333XQ
    	char caracter;
    	if(!(matricula.length()==7))
    		throw new RuntimeException("Se ha ingresado un valor de patente invalido");
	    	else { 
	    		for(int i=0; i< matricula.length();i++) {
	    		caracter = matricula.charAt(i);
	    		if(!((i<=1 && caracter>='A'&& caracter<='Z')||(1<i&&i<=4 && Character.isDigit(caracter))||(4<i&&i<=6 && caracter>='A'&& caracter<='Z') ))//tiene que cumplir con el formato de patente
	    			throw new RuntimeException("Se ha ingresado un valor de patente invalido");
	    		}
	    	}
    	if(!(0<cargaMaxPeso ))//valor maximo permitido en argentina && cargaMaxPeso<=52500
    		throw new RuntimeException("Se ha ingresado una carga maxima de peso del transporte incorrecto");
    	if(!(0<cargaMaxVol)) //valor maximo permitido en argentina && cargaMaxVol<=213.2
    		throw new RuntimeException("Se ha ingresado una carga maxima de volumen del transporte incorrecto");
    	if(!(0.0<costoKmViaje ))
    		throw new RuntimeException("Se ha ingresado un costo por km de viaje negativo");
    }
    public void asignarUnDestino(Destino dest){
    	if(conCarga==false && enViaje==false)
    		this.destinoDelTransporte=dest;
    	else {
    		throw new RuntimeException("El transporte est� en viaje y/o ya tiene carga");
    	}
    }

    public boolean cargarPaquete(Paquete paquete) {
    	
    	if (this.destinoDelTransporte==null || this.enViaje==true) {
    		throw new RuntimeException();
    	}
    	else {
    		if(paquete ==null ) {
    			return false;
    		}
    		else if((this.cargaMaxPesoActual+paquete.darPeso())<=this.cargaMaxPeso && (this.cargaMaxVolActual+paquete.darVol())<=this.cargaMaxVol) {
    			paquetes.add(paquete);
    			this.cargaMaxPesoActual=this.cargaMaxPesoActual+paquete.darPeso();
    			this.cargaMaxVolActual=this.cargaMaxVolActual+paquete.darVol();
    			this.conCarga=true;
    			return true;
    		}
    		else {
    			return false;
    		}
    	}
    }
    public String destinoTransoporte() {
    	if (this.destinoDelTransporte==null) {
    		throw new RuntimeException("El transporte no tiene un destino asignado");
    	}
    	else {
    		return this.destinoDelTransporte.darDestino();
    	}
    }
    public boolean estadoCarga() {
        return this.conCarga;
    }

    public void empezarElViaje() {
    	if (this.enViaje) {
    		throw new RuntimeException();
    	}
    	else {
    		if(paquetes.isEmpty() || this.destinoDelTransporte==null) {
    			throw new RuntimeException();
    		}
    		else {
    			this.enViaje=true;
    		}
    	}
    }
    public void terminarElViaje() {
    	if(this.enViaje) {
    		this.destinoDelTransporte=null;
    		this.vaciarTransporte();
    		this.enViaje=false;
    	}
    	else {
    		throw new RuntimeException();
    	}
    }

    public void blanquearViaje() {
    }
    public double costoTotalViaje() {
    	if(enViaje) {
    		double costo;
    		costo=this.costoKmViaje*destinoDelTransporte.darDistanciaKm();
    		return costo;
    	}
    	else {
    		throw new RuntimeException();
    	}
    }

    public void vaciarTransporte() {
    	paquetes.clear();
    	this.conCarga=false;
    	this.cargaMaxPesoActual=0.0;
    	this.cargaMaxVolActual=0.0;
    }
    public boolean tieneEspacioLibre() {
    	if(this.cargaMaxVolActual<this.cargaMaxVol && this.cargaMaxPesoActual<this.cargaMaxPeso) {
    		return true;
    	}
    	else
    		return false; 
    }
    public boolean refrigeracionDelTransporte() {
    	return this.refrigeracion;
    }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((destinoDelTransporte == null) ? 0 : destinoDelTransporte
						.hashCode());
		result = prime * result
				+ ((paquetes == null) ? 0 : paquetes.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transporte other = (Transporte) obj;
		if (destinoDelTransporte == null) {
			if (other.destinoDelTransporte != null)
				return false;
		} else if (!destinoDelTransporte.equals(other.destinoDelTransporte))
			return false;
		if (paquetes == null) {
			if (other.paquetes != null)
				return false;
		} else if (!paquetes.equals(other.paquetes))
			return false;
		return true;
	}
	public String darMatricula() {
		return matricula;
	}
	@Override
	public String toString() {
		StringBuilder st=new StringBuilder();
		st.append("Matricula: ");
		st.append(matricula);
		st.append(", el transporte es de tipo: ");
		st.append(this.getClass().getSimpleName());
		st.append(", la carga maxima en peso es: ");
		st.append(cargaMaxPeso);
		st.append(", la carga maxima en volumen es: ");
		st.append(cargaMaxVol);
		st.append(", el destino del transporte es: ");
		st.append(destinoDelTransporte);
		return st.toString();
	}
    
}