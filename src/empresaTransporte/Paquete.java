package empresaTransporte;


public class Paquete {
	private String destino;
	private double peso;
	private double volumen;
	private boolean refrigeracion;
	
    public Paquete() {
    }
   
    public Paquete(String destino,double peso,double volumen, boolean refrigeracion) {
    	this.destino=destino;
    	this.peso=peso;
    	this.volumen=volumen;
    	this.refrigeracion=refrigeracion;
    	invarianteRep();
    }
    private void invarianteRep() {    	
    	if(!(0.0<peso))
    		throw new RuntimeException("Se ha ingresado un peso de paquete invalido");
    	if(!(0.0<volumen)) 
    		throw new RuntimeException("Se ha ingresado un volumen de paquete invalido");    	
    	
    }
    public String darDestino() {
        return this.destino;
    }

    public double darPeso() {
        return this.peso;
    }

    public double darVol() {
        return this.volumen;
    }

    public boolean darRefrigeracion() {
        return this.refrigeracion;
    }

	@Override
	public String toString() {
		return " ";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		long temp;
		temp = Double.doubleToLongBits(peso);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (refrigeracion ? 1231 : 1237);
		temp = Double.doubleToLongBits(volumen);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paquete other = (Paquete) obj;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (Double.doubleToLongBits(peso) != Double.doubleToLongBits(other.peso))
			return false;
		if (refrigeracion != other.refrigeracion)
			return false;
		if (Double.doubleToLongBits(volumen) != Double.doubleToLongBits(other.volumen))
			return false;
		return true;
	}

	
    
}