package empresaTransporte;
import java.math.BigInteger;

import java.util.HashMap;
import java.util.Map;
//import java.util.HashSet;
//import java.util.TreeSet;

public class Empresa {
	private String cuit;
    private String nombre;
    private Deposito depoRefri;
    private Deposito depoSinRefri;
    private HashMap <String,Transporte> transportes;
    private HashMap <String, Destino> destinos;
    
    public Empresa() {
    }
    public Empresa(String cuit, String nombre, double capacidadDeCadaDeposito) {
    	this.cuit=cuit;
    	this.nombre=nombre;
    	depoRefri=new DepositoConRefrigeracion(capacidadDeCadaDeposito,true);
    	depoSinRefri=new DepositoSinRefrigeracion(capacidadDeCadaDeposito, false);
    	transportes=new HashMap<String, Transporte>();
    	destinos=new HashMap <String,Destino>();
    	invarianteRep();
    }
    private void invarianteRep() {
    	//el cuit debe contener un formato de tipo cuit con 11 digitos sin guiones    	
    	try {
    		BigInteger numCuit = new BigInteger(cuit);    		
        	BigInteger cotaMenor = new BigInteger("30000000000");
    		BigInteger cotaMayor = new BigInteger("40000000000");
            if ((numCuit.compareTo(cotaMenor)==1  && numCuit.compareTo(cotaMayor)==-1)==false)//devuelve 1 si es mayor o igual y -1 si es menor
            	throw new RuntimeException ("El cuit ingresado no pertenece a un cuit de empresa");
        }
        catch (NumberFormatException ex){//captura error si el cuit no cumple con un string de todos digitos 
            ex.printStackTrace();
            throw new RuntimeException ("El valor ingresado no pertenece a un Cuit");
        }   	
    	//tiene que suceder que la clave del diccionrio transportes tiene que conincidir con el atributo  
    	//destino del valor del diccionario
    	for (String clave : destinos.keySet()) {    		
            Destino viaje = destinos.get(clave);
            if(!(clave.equals(viaje.darDestino())))
            	throw new RuntimeException ("Error en la base de datos destinos.");
        }
    	//tiene que suceder que la clave del diccionrio transportes tiene que conincidir con el atributo  
    	//matricula del valor del diccionario
    	for (String clave : transportes.keySet()) {    		
    		Transporte transporte = transportes.get(clave);
            if(!(clave.equals(transporte.darMatricula())))
            	throw new RuntimeException ("Error en la base de datos transportes.");
        }
    	   	
    	    	
    }
    public boolean incorporarPaquete(String destino,double peso,double volumen, boolean refrigeracion) {
    	if (destinos.containsKey(destino)) {    	
	    	Paquete envio;
	    	envio=new Paquete(destino,peso,volumen,refrigeracion);
	    	if(refrigeracion) {
	    		return depoRefri.incorpararPaqueteDep(envio);
	    	}
	    	else {
	    		return depoSinRefri.incorpararPaqueteDep(envio);
	    	}	    	    	
    	}
    	return false;
    }
    public void asignarDestino(String matricula, String destino) { 
    	Destino dest=this.destinos.get(destino);    	
    	Transporte trans=this.transportes.get(matricula);
    	if (destinos.containsKey(destino)) {
    		trans.asignarUnDestino(dest);
    	}
    	else {
    		throw new RuntimeException("El destino no est� registrado");
    	}
    }
    public double cargarTransporte(String matricula) {
    	double vol=0.0;
    	Transporte trans=transportes.get(matricula);
    	if(trans.refrigeracionDelTransporte()) {
    		Paquete envio=depoRefri.darUnPaqueteConElMismoDestino(trans.destinoTransoporte());
    		while(trans.tieneEspacioLibre() && envio != null) {
    			trans.cargarPaquete(envio);
    			vol+=envio.darVol();
    			envio=depoRefri.darUnPaqueteConElMismoDestino(trans.destinoTransoporte());
    		}
    	}
    	else {
    		Paquete envio=depoSinRefri.darUnPaqueteConElMismoDestino(trans.destinoTransoporte());
    		while(trans.tieneEspacioLibre() && envio != null) {
    			trans.cargarPaquete(envio);
    			vol=vol+envio.darVol();
    			envio=depoSinRefri.darUnPaqueteConElMismoDestino(trans.destinoTransoporte());
    		}
    	}
        return vol;
    }
    public void iniciarViaje(String matricula) {
    	Transporte trans;
    	trans=transportes.get(matricula);
    	trans.empezarElViaje();
    }

    public void finalizarViaje(String matricula) {
    	Transporte trans;
    	trans=transportes.get(matricula);
    	trans.terminarElViaje();
    }

    public double obtenerCostoViaje (String matricula) {
    	Transporte trans=transportes.get(matricula);
        return trans.costoTotalViaje();
    }

    public void agregarDestino(String destino, int km)  {
    	Destino viaje;
    	viaje=new Destino(destino,km);
    	if(this.destinos.containsKey(destino)) {
    		throw new RuntimeException("Ha ingresado que ya se encuentra");
    	}
    	else {
    		this.destinos.put(destino, viaje);
    	}
    }

    public void agregarTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm, double segCarga) {
    	TrailerComun camion;
    	camion= new TrailerComun(matricula,cargaMax,capacidad,tieneRefrigeracion,costoKm,segCarga);
    	this.transportes.put(matricula, camion);
    }

    public void agregarMegaTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm, double segCarga, double costoFijo, double costoComida) {
    	MegaTrailer superCamion;
    	superCamion=new MegaTrailer(matricula,cargaMax,capacidad,tieneRefrigeracion,costoKm,segCarga,costoFijo,costoComida);
    	this.transportes.put(matricula, superCamion);
    }
    public void agregarFlete(String matricula, double cargaMax, double capacidad, double costoKm, int cantAcompaniantes, double costoPorAcompaniante) {
    	Flete flete;
    	flete=new Flete(matricula,cargaMax,capacidad,costoKm,cantAcompaniantes,costoPorAcompaniante);
    	this.transportes.put(matricula, flete);
    }

    public String obtenerTransporteIgual(String matricula) {
    	Transporte transporte1=this.transportes.get(matricula);
    	for(Map.Entry<String, Transporte> entrada :transportes.entrySet()) {
    		Transporte transporte2=entrada.getValue();
    		if(transporte1.equals(transporte2) && !(matricula.compareTo(transporte2.darMatricula())==0)){
    			return transporte2.darMatricula();
    		}
    	}
        return null;
    }
	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append("Cuit= ");
		st.append(cuit);
		st.append(", Nombre= ");
		st.append(nombre);
		st.append(", Transportes= ");
		st.append(transportes.values());
		return st.toString();
	}
}