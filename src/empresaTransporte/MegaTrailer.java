package empresaTransporte;


public class MegaTrailer extends Transporte {
	
	private double seguroCarga;
    private double gastoComida;
    private double costoFijoViaje;
	
    public MegaTrailer() {
    }
    
    @Override
    public double costoTotalViaje() {
    	if(enViaje) {
    		return super.costoTotalViaje()+this.seguroCarga+gastoComida+costoFijoViaje;
    	}
    	else {
    		throw new RuntimeException("El trasnporte no est� en viaje");
    	}
    }
    @Override
    public void asignarUnDestino(Destino dest) {
    	if(dest.darDistanciaKm()<500) {
    		throw new RuntimeException("El destino est� muy cerca para este tipo de transporte");
    	}
    	else {
    		super.asignarUnDestino(dest);
    	}
    }

    public MegaTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm, double segCarga, double costoFijo, double costoComida) {
    	super(matricula,cargaMax,capacidad,tieneRefrigeracion,costoKm);
    	seguroCarga=segCarga;
    	gastoComida=costoComida;
    	costoFijoViaje=costoFijo; 
    	invarianteRep();

    }
    private void invarianteRep() {
    	if(!(0<=seguroCarga))
    		throw new RuntimeException("Se ha ingresado un seguro de carga invalido");
    	if(!(0<=gastoComida))
    		throw new RuntimeException("Se ha ingresado un gasto de Comida  invalido");
    	if(!(0<=costoFijoViaje))
    		throw new RuntimeException("Se ha ingresado un costo fijo de viaje invalido");
    }
}