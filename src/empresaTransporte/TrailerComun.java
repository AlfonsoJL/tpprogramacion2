package empresaTransporte;



public class TrailerComun extends Transporte {
	
	private double seguroCarga;
	
    public TrailerComun() {
    }
    @Override
    public double costoTotalViaje() {
    	if(enViaje) {
    		return super.costoTotalViaje()+this.seguroCarga;
    	}
    	else {
    		throw new RuntimeException("El transporte no est� en viaje");
    	}
    }
    @Override
    public void asignarUnDestino(Destino dest) {
    	if(dest.darDistanciaKm()>=500) {
    		throw new RuntimeException("El destino est� muy lejos para este tipo de transporte");
    	}
    	else {
    		super.asignarUnDestino(dest);
    	}
    }

    public TrailerComun(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm, double segCarga) {
    	super(matricula,cargaMax,capacidad,tieneRefrigeracion,costoKm);
    	seguroCarga=segCarga;
    	invarianteRep();
    }
    private void invarianteRep() {
    	if(!(0<seguroCarga))
    		throw new RuntimeException("Se ha ingresado un seguro de carga invalido");
    	}
}