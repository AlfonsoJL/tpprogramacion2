package empresaTransporte;

import java.util.Iterator;
import java.util.HashSet;

public abstract class Deposito{
	protected HashSet <Paquete> paquetes;
    private boolean refrigeracion;
    private double capacidadMaxDep;
    private double capacidadActual;

	public Deposito(double capacidadMaxDep, boolean refrigeracion) {
    	this.capacidadMaxDep=capacidadMaxDep;
    	this.refrigeracion=refrigeracion;
    	paquetes=new HashSet<Paquete>();
    	invarianteRep();
    }
	private void invarianteRep() {//la capacidad del deposito tiene que cumplir con un volumen adecuado
		if (!(0< capacidadMaxDep))
    		throw new RuntimeException("Ingrese un volumen de deposito correcto");		
	}	
	
    public boolean incorpararPaqueteDep(Paquete envio) {
    	if((this.capacidadActual+envio.darVol())<=capacidadMaxDep) {
    		this.paquetes.add(envio);
    		this.capacidadActual=this.capacidadActual+envio.darVol();
    		return true;
    	}
    	else return false;
    }
    public Paquete darUnPaqueteConElMismoDestino(String destino) {
    	Iterator<Paquete> iterador=this.paquetes.iterator();
    	while(iterador.hasNext()) {
    		Paquete envio=iterador.next();
    		if(envio.darDestino().compareTo(destino) == 0) {
    			iterador.remove();
    			return envio;
    		}
    	}
    	return null;
    }
    
}